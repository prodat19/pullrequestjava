package aytocalc;

public class Arcotangente {

	public double calcula(double a, double b) {
		
		double resultado = Math.atan2(a, b);
		return resultado; 
		
	}
	
}
